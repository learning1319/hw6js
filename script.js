/* 1. Екранування надає можлиивість використовувати спецсимволи, які входять до синтаксису JS, а також спецсимволи.
2. Декларування, створення функціонального виразу, стрілкова функція.
3. hoisting - підняття. При оголошенні функції декларуванням, її можна оголошувати раніше по коду, ніж вона прописана в ньому.
*/
"use strict"

const createNewUser = () =>{
    
  let newUser = {
        firstName: prompt("Enter your name."),
        lastName: prompt("Enter your surname."),
        birthday: prompt("Enter your birthday", "dd.mm.yyyy"),
        getAge(){
            
             let now = new Date();
             let rightBirthday = new Date(this.birthday.slice(-4) + ','+ this.birthday.slice(3,5) + ','+ this.birthday.slice(0,2));
             let userAge = new Date(now - rightBirthday).getFullYear() - 1970;
             return userAge;
        },
       getLogin() {
             return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;

        },
        getPassword() {
            return`${this.firstName[0].toUpperCase()}${this.lastName.slice(0).toLowerCase()}${this.birthday.slice(-4)}`
        }
    }
     return newUser;

};
const user = createNewUser();
console.log(user)
console.log(user.getAge());
console.log(user.getPassword())
